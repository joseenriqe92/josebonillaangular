export interface Crypto {
	id: number;
	abbreviation: string;
	name: string;
	value: number;
	percentage: number;
}